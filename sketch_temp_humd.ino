#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DHT_U.h>

int SENSOR = 40;
int tem1 = 0;
int hum1 = 0;

DHT dht1 (SENSOR, DHT11);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  dht1.begin();
}

void loop(){
  // put your main code here, to run repeatedly:
  tem1 = dht1.readTemperature();
  hum1 = dht1.readHumidity();

  Serial.print("Temperatura: ");
  Serial.print(tem1);
  Serial.print("°C Humedad: ");
  Serial.print(hum1);
  Serial.println("%");

  delay(1000);
}
